from unittest import TestCase

from client.constants import Selection
from client.trello_clone import TrelloClient


class SelectionTest(TestCase):
    def test_values(self):
        self.assertEqual(Selection.values(), {'1', 'a', 'c', 'b', 'e', 'd', 'g', 'f', 'i', 'h', 'k', 'j', 'm', 'l',
                                              'o', 'n', 'q', 'p', 's', 'r', 'u', 't', 'w', 'v', 'y', 'x', 'z'})


class TrelloClientTest(TestCase):
    def test_(self):
        trello_client = TrelloClient()
