import sys
from datetime import datetime

from constants import Selection
from entities.utils import print_menu_options, get_user_int_input, get_user_string_input


class Cards(list):
    def __init__(self):
        super(list, self).__init__()
        self.current_card_index = -1
        self.current_card = None

    def list_cards(self):
        sys.stdout.write('\n')
        for count, card in enumerate(self):
            if not card.archived:
                sys.stdout.write('\n%s.)\n' % count)
                card.print_card_details()
                sys.stdout.write('\n')
        sys.stdout.write('\n')

    def add_card(self, card):
        self.append(card)
        self.current_card = self[-1]
        self.current_card_index = len(self) - 1

    def print_options(self):
        sys.stdout.write('\n\nCard Options:')
        print_menu_options([('Select a card.', Selection.SELECT_CARD), ('Create a card.', Selection.CREATE_CARD),
                            ('Rename a card.', Selection.RENAME_CARD), ('Archive a card.', Selection.ARCHIVE_CARD),
                            ('Reorder a card.', Selection.REORDER_CARD),
                            ('Move card to other list.', Selection.MOVE_CARD), ('List cards.', Selection.LIST_CARDS)])

        if self.current_card:
            self.current_card.print_card_instance_options()


    def execute_option(self, selection, board_labels, members):
        if selection == Selection.SELECT_CARD:
            self.list_cards()
            self.current_card_index = get_user_int_input('\nEnter the ID of the card to select>>>')
            self.current_card = self[self.current_card_index]

        if selection == Selection.CREATE_CARD:
            card_title = get_user_string_input('\nEnter card title>>>')
            self.add_card(Card(card_title))
            sys.stdout.write('Card created successfully\n')

        if selection == Selection.RENAME_CARD:
            self.list_cards()
            card_index = get_user_int_input('\nEnter the ID of the card to select>>>')
            card_title = get_user_string_input('\nEnter card title>>>')
            self[card_index].rename(card_title)

        if selection == Selection.ARCHIVE_CARD:
            self.list_cards()
            card_index = get_user_int_input('\nEnter the ID of the card to select>>>')
            self[card_index].archive()

        if selection == Selection.LIST_CARDS:
            self.list_cards()

        if selection == Selection.CARD_WRITE_DESCRIPTION:
            card_description = get_user_string_input('\nEnter a description for the card>>>')
            self.current_card.set_description(card_description)

        if selection == Selection.CARD_DUE_DATE:
            valid_data = False
            while not valid_data:
                try:
                    card_due_date = get_user_string_input('\nEnter a due date for the card (YYYY/mm/dd_HH:MM:SS)>>>')
                    self.current_card.set_due_date(datetime.strptime(card_due_date, '%Y/%m/%d_%H:%M:%S'))
                    valid_data = True
                except ValueError:
                    sys.stderr.write('<<bad format, follow the format YYYY/mm/dd_HH:MM:SS >>')

        if selection == Selection.CARD_ASSIGN_LABEL:
            board_labels.list_labels()
            label_id = get_user_int_input('\nEnter the ID of the label to assign>>>')
            self.current_card.add_label(board_labels[label_id])

        if selection == Selection.CARD_ASSIGN_MEMBER:
            members.list_members()
            member_id = get_user_int_input('\nEnter the ID of the member to assign>>>')
            self.current_card.add_member(members[member_id])


class Card(object):
    def __init__(self, title, archived=False):
        self.title = title
        self.archived = archived
        self.description = ''
        self.due_date = ''
        self.label = None
        self.members = set([])

    def archive(self):
        self.archived = True

    def rename(self, title):
        self.title = title

    def add_member(self, member):
        self.members.add(member)

    def add_label(self, label):
        self.label = label

    def set_description(self, description):
        self.description = description

    def set_due_date(self, due_date):
        self.due_date = due_date

    def print_card_instance_options(self):
        sys.stdout.write('\n\nOptions for card %s:' % self.title)
        print_menu_options([('Assign a member.', Selection.CARD_ASSIGN_MEMBER),
                            ('Assign a label.', Selection.CARD_ASSIGN_LABEL),
                            ('Write a description.', Selection.CARD_WRITE_DESCRIPTION),
                            ('Set a due date.', Selection.CARD_DUE_DATE)])

    def print_card_details(self):
        sys.stdout.write('\nTitle: %s, Archived: %s, Label: %s' % (self.title, self.archived, self.label))
        sys.stdout.write('\nDue Date: %s' % self.due_date)
        sys.stdout.write('\nDescription: %s' % self.description)
        sys.stdout.write('\nMembers:')
        for member in self.members:
            sys.stdout.write('\n\t%s' % member)

    def __str__(self):
        return 'Title: %s, Archived: %s' % (self.title, self.archived)
