import sys

from constants import Selection
from utils import print_menu_options, get_user_int_input, get_user_string_input
from entities.card import Cards


class Lists(list):
    def __init__(self):
        super(list, self).__init__()
        self.current_list_index = -1
        self.current_list = None

    @staticmethod
    def print_options():
        sys.stdout.write('\nList Options:')
        print_menu_options([('Select a list.', Selection.SELECT_LIST), ('Create a list.', Selection.CREATE_LIST),
                            ('Rename a list.', Selection.RENAME_LIST), ('Archive a list.', Selection.ARCHIVE_LIST),
                            ('Reorder a list.', Selection.REORDER_LIST), ('List lists.', Selection.LIST_LISTS)])

    def list_lists(self):
        sys.stdout.write('\n')
        for count, board_list in enumerate(self):
            if not board_list.archived:
                sys.stdout.write('\n%s.) %s\n' % (count, board_list))
        sys.stdout.write('\n')

    def add_list(self, title):
        self.append(title)
        self.current_list = self[-1]
        self.current_list_index = len(self) - 1

    def execute_option(self, selection):
        if selection == Selection.SELECT_LIST:
            if not len(self):
                sys.stdout.write('\nNo lists exist.\n')
                return
            self.list_lists()
            self.current_list_index = get_user_int_input('\nEnter the ID of the list to select>>>')
            self.current_list = self[self.current_list_index]

        if selection == Selection.CREATE_LIST:
            list_title = get_user_string_input('\nEnter list title>>>')
            self.add_list(List(list_title))
            self.current_list_index = len(self) - 1
            sys.stdout.write('List created successfully\n')

        if selection == Selection.RENAME_LIST:
            self.list_lists()
            list_index = get_user_int_input('\nEnter the ID of the list to select>>>')
            list_title = get_user_string_input('\nEnter list title>>>')
            self[list_index].rename(list_title)

        if selection == Selection.ARCHIVE_LIST:
            self.list_lists()
            list_index = get_user_int_input('\nEnter the ID of the list to select>>>')
            self[list_index].archive()

        if selection == Selection.LIST_LISTS:
            self.list_lists()

        if selection == Selection.MOVE_CARD:
            self.current_list.cards.list_cards()
            card_id = get_user_int_input('\nEnter the ID of the card to select>>>')
            self.list_lists()
            list_id = get_user_int_input('\nEnter the ID of the list to send the card to>>>')
            self.move_card(card_id, self.current_list,  list_id)

        if selection == Selection.REORDER_CARD:
            self.current_list.cards.list_cards()
            card_id = get_user_int_input('\nEnter the ID of the card to select>>>')
            new_position = get_user_int_input('\nEnter the new position number>>>')
            self.current_list.reorder_cards(card_id, new_position)

    def move_card(self, card_id, from_list_id,  to_list_id):
        self[to_list_id].add_card(from_list_id.cards[card_id])
        temp_cards = from_list_id.cards[:card_id] + from_list_id.cards[card_id:]
        from_list_id.cards = Cards()
        for card in temp_cards:
            from_list_id.cards.add_card(card)


class List(object):
    def __init__(self, title, archived=False):
        self.title = title
        self.archived = archived
        self.cards = Cards()

    def archive(self):
        self.archived = True

    def rename(self, title):
        self.title = title

    def add_card(self, card):
        self.cards.append(card)

    def reorder_cards(self, card_position, new_position):
        if card_position >= len(self.cards) or new_position >= len(self.cards):
            return False
        reordered_card = self.cards[card_position]
        order_card = Cards()
        order_card.add_card(reordered_card)
        temp_list = self.cards[:card_position] + self.cards[card_position + 1:]
        temp = temp_list[:new_position] + order_card + temp_list[new_position:]
        self.cards = Cards()
        for card in temp:
            self.cards.add_card(card)

    def __str__(self):
        return 'Title: %s, Archived: %s' % (self.title, self.archived)