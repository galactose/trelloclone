import sys

from constants import Selection
from utils import print_menu_options, get_user_int_input, get_user_string_input
from entities.list import Lists
from entities.label import Label, Labels


class TooManyLabels(Exception):
    pass


class Boards(list):
    def __init__(self):
        super(list, self).__init__()
        self.current_board_index = -1
        self.current_board = None

    def list_boards(self):
        sys.stdout.write('\n')
        for count, board in enumerate(self):
            if not board.archived:
                sys.stdout.write('\n%s.) %s\n' % (count, board))
        sys.stdout.write('\n')

    def set_board(self, index):
        self.current_board_index = index
        self.current_board = self[index]

    def create_board(self, title):
        self.append(Board(title))
        self.current_board = self[-1]
        self.current_board_index = len(self) - 1

    def print_board_options(self):
        sys.stdout.write('\nBoard Options:')
        print_menu_options([('List boards.', Selection.LIST_BOARDS), ('Select a board.', Selection.SELECT_BOARD),
                            ('Create a board.', Selection.CREATE_BOARD), ('Rename a board.', Selection.RENAME_BOARD),
                            ('Archive a board.', Selection.ARCHIVE_BOARD)])

        if self.current_board:
            print_menu_options([('Add a label.', Selection.ADD_LABEL_TO_BOARD),
                                ('List labels.', Selection.LIST_LABELS) ])

    def execute_option(self, selection):
        if selection == Selection.SELECT_BOARD:
            if not len(self):
                sys.stdout.write('\nNo boards exist.\n')
                return
            self.list_boards()
            select_board = get_user_int_input('\nEnter the ID of the board to select>>>')
            self.set_board(select_board)
        if selection == Selection.CREATE_BOARD:
            board_title = get_user_string_input('\nEnter board title>>>')
            self.create_board(board_title)
            sys.stdout.write('Board created successfully\n')
        if selection == Selection.RENAME_BOARD:
            board_title = get_user_string_input('\nEnter board title>>>')
            self.current_board.rename(board_title)
        if selection == Selection.ARCHIVE_BOARD:
            self.current_board.archive()
        if selection == Selection.LIST_BOARDS:
            self.list_boards()
        if selection == Selection.REORDER_LIST:
            self.current_board.lists.list_lists()
            list_id = int(get_user_int_input('\nEnter the ID of the list to select>>>'))
            new_position = get_user_int_input('\nEnter the new position number>>>')
            self.reorder_lists(list_id, new_position)
        if self.current_board and selection == Selection.ADD_LABEL_TO_BOARD:
            label_title = get_user_string_input('\nEnter label title>>>')
            try:
                self.current_board.add_label(Label(label_title))
            except TooManyLabels:
                sys.stderr.write('\n<<This board has too many labels>>\n')
        if self.current_board and selection == Selection.LIST_LABELS:
            self.current_board.labels.list_labels()

    def reorder_lists(self, list_position, new_position):
        if list_position >= len(self.current_board.lists) or new_position >= len(self.current_board.lists):
            return False
        reordered_list = self.current_board.lists[list_position]
        order_list = Lists()
        order_list.add_list(reordered_list)
        temp_list = self.current_board.lists[:list_position] + self.current_board.lists[list_position + 1:]
        temp = temp_list[:new_position] + order_list + temp_list[new_position:]
        self.current_board.lists = Lists()
        for board_list in temp:
            self.current_board.lists.add_list(board_list)


class Board(object):
    def __init__(self, title, archived=False):
        self.title = title
        self.archived = archived
        self.lists = Lists()
        self.labels = Labels()

    def archive(self):
        self.archived = True

    def rename(self, title):
        self.title = title

    def add_label(self, label):
        if len(self.labels) > 5:
            raise TooManyLabels
        self.labels.append(label)

    def __str__(self):
        return 'Title: %s, Archived: %s' % (self.title, self.archived)
