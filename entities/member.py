import sys

from constants import Selection
from entities.utils import print_menu_options, get_user_string_input, get_user_int_input


class Members(list):
    def __init__(self):
        super(list, self).__init__()

    def list_members(self):
        sys.stdout.write('\n')
        for count, member in enumerate(self):
            if not member.archived:
                sys.stdout.write('\n%s.) %s\n' % (count, member))
        sys.stdout.write('\n')

    @staticmethod
    def print_options():
        print_menu_options([('List members.', Selection.LIST_MEMBERS), ('Create a member.', Selection.CREATE_MEMBER),
                            ('Rename a member.', Selection.RENAME_MEMBER),
                            ('Archive a member.', Selection.ARCHIVE_MEMBER)])

    def execute_option(self, selection):
        if selection == Selection.CREATE_MEMBER:
            member_title = get_user_string_input('\nEnter member title>>>')
            self.append(Member(member_title))

        if selection == Selection.RENAME_MEMBER:
            self.list_members()
            member_id = get_user_int_input('\nEnter the ID of the member to select>>>')
            member_name = get_user_string_input('\nEnter a new member name>>>')
            self[member_id].rename(member_name)

        if selection == Selection.ARCHIVE_MEMBER:
            self.list_members()
            member_id = get_user_int_input('\nEnter the ID of the member to select>>>')
            self[member_id].archive()

        if selection == Selection.LIST_MEMBERS:
            self.list_members()


class Member(object):
    def __init__(self, name, archived=False):
        self.name = name
        self.archived = archived

    def archive(self):
        self.archived = True

    def rename(self, name):
        self.name = name

    def __str__(self):
        return 'Name: %s, Archived: %s' % (self.name, self.archived)

    def __hash__(self):
        return hash(self.name)
