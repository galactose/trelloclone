import sys

from constants import Selection


def print_menu_options(option_list):
    for menu_option_letter, option_text in option_list:
        sys.stdout.write('\n\t%s.) %s' % (option_text, menu_option_letter))


def get_user_selection():
    selection = None
    while not selection:
        try:
            selection = raw_input('User Selection>>>')
        except ValueError:
            pass
        if selection not in Selection.values():
            selection = None
    return selection


def get_user_string_input(prompt):
    string_input = None
    while not string_input:
        try:
            string_input = raw_input(prompt)
        except ValueError:
            pass
    return string_input


def get_user_int_input(prompt):
    int_input = None
    while int_input is None:
        try:
            int_input = int(raw_input(prompt))
        except ValueError:
            pass
    return int_input
