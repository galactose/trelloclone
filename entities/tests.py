from unittest import TestCase

from entities.board import Board, Boards, TooManyLabels
from entities.list import List
from entities.label import Label


class BoardTest(TestCase):
    def setUp(self):
        self.board = Board('test_board')

    def test_archive(self):
        self.board.archive()
        self.assertTrue(self.board.archived)

    def test_rename(self):
        self.assertEqual(self.board.title, 'test_board')
        self.board.rename('still_test_board')
        self.assertEqual(self.board.title, 'still_test_board')

    def test_add_label(self):
        labels = [Label('Very High Priority'), Label('High Priority'), Label('Medium Priority'),
                  Label('Low Priority'), Label('Very Low Priority'), Label('No Priority')]
        for label in labels:
            self.board.add_label(label)
        self.assertEqual(self.board.labels, labels)
        self.assertRaises(TooManyLabels, self.board.add_label, Label('Test Priority'))


class BoardsTest(TestCase):
    def setUp(self):
        self.board = Boards()


class ListTest(TestCase):
    def setUp(self):
        self.board_list = List('test_list')


class ListsTest(TestCase):
    def setUp(self):
        self.board_list = List('test_list')

