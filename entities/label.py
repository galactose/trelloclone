
class Labels(list):
    def __init__(self):
        super(list, self).__init__()

    def list_labels(self):
        for count, label in enumerate(self):
            print '%s.) %s' % (count, label)


class Label(object):
    def __init__(self, title):
        self.title = title

    def rename(self, title):
        self.title = title

    def __hash__(self):
        return hash(self.title)

    def __str__(self):
        return 'Title: %s.' % self.title
