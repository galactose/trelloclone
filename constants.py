
class Selection(object):
    EXIT = 'x'
    LIST_BOARDS = 'a'
    SELECT_BOARD = 'b'
    CREATE_BOARD = 'c'
    RENAME_BOARD = 'd'
    ARCHIVE_BOARD = 'e'

    LIST_LISTS = 'f'
    SELECT_LIST = 'g'
    CREATE_LIST = 'h'
    RENAME_LIST = 'i'
    ARCHIVE_LIST = 'j'
    REORDER_LIST = 'k'

    SELECT_CARD = 'l'
    CREATE_CARD = 'm'
    RENAME_CARD = 'n'
    ARCHIVE_CARD = 'o'
    REORDER_CARD = 'p'
    MOVE_CARD = 'q'
    CARD_ASSIGN_MEMBER = 'r'
    CARD_ASSIGN_LABEL = 's'
    CARD_WRITE_DESCRIPTION = 't'
    CARD_DUE_DATE = 'u'
    LIST_CARDS = 'v'

    CREATE_MEMBER = 'w'
    RENAME_MEMBER = 'y'
    ARCHIVE_MEMBER = 'z'
    LIST_MEMBERS = '1'
    ADD_LABEL_TO_BOARD = '2'
    LIST_LABELS = '3'

    @staticmethod
    def values():
        return {value for key, value in Selection.__dict__.items() if not key.startswith('__') and key != 'values'}
