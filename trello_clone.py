import sys

from constants import Selection
from entities.board import Boards
from entities.member import Members
from entities.label import Labels
from entities.utils import get_user_selection


class TrelloClient(object):

    def __init__(self):

        self.boards = Boards()
        self.members = Members()
        self.labels = Labels()

    def run_client(self):
        """
        Runs the main loop of the Trello simulator
        """
        while True:
            self.print_main_page_options()
            if self.boards.current_board:
                self.boards.current_board.lists.print_options()
                if self.boards.current_board.lists.current_list:
                    self.boards.current_board.lists.current_list.cards.print_options()
            self.print_current_user_status()
            selection = get_user_selection()
            if selection == Selection.EXIT:
                break
            self.execute_command(selection)

    def execute_command(self, selection):
        """
        Given a user selection, action the task and return back to the main program loop.
        """

        self.boards.execute_option(selection)
        self.members.execute_option(selection)
        if self.boards.current_board:
            self.boards.current_board.lists.execute_option(selection)
            if self.boards.current_board.lists.current_list:
                self.boards.current_board.lists.current_list.cards.execute_option(
                    selection, self.boards.current_board.labels, self.members)

    def print_main_page_options(self):
        sys.stdout.write('Welcome to Trello Clone!\nPlease make a selection:\n')
        self.boards.print_board_options()
        self.members.print_options()
        sys.stdout.write('\n\t%s.) Exit.\n' % Selection.EXIT)

    def print_current_user_status(self):
        """Print the board the member is on, what list they are looking at and the card the member is focusing on."""

        board_list = None
        list_card = None
        if self.boards.current_board:
            board_list = self.boards.current_board.lists.current_list
            if self.boards.current_board.lists.current_list:
                list_card = self.boards.current_board.lists.current_list.cards.current_card

        for entity in [('board', self.boards.current_board), ('list', board_list), ('card', list_card)]:
            if not entity[1]:
                out_text = '\n<<User is not currently on a %s.>>' % entity[0]
            else:
                out_text = '\n<<User is on %s %s.>>' % (entity[0], entity[1].title)
            sys.stdout.write(out_text)
        sys.stdout.write('\n')


if __name__ == '__main__':
    TrelloClient().run_client()
